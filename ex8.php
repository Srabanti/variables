<?php
$name = 'srabanti';
echo '1. my name is srabanti <br />'; // output: my name is srabanti
echo "2. my name is $name <br />"; // output: my name is susie
echo "3. my name is ${name}<br />"; // output: my name is susie
echo "4. my name is . $name . <br />"; // output: my name is susie
echo '5. my name is $name <br />'; // output: my name is $name
                                // single quotes shows the text as it is, no parsing.
                               // double quotes parses the text and returns the output.
                               // can you guess what parse means?
?>
